import { HEROES } from '../models/heroes';

export const getHeroes = (req, res) => {
    res.json(HEROES);
};
export const getHero = (req, res) => {
    const id = +req.params.name;
    console.log(id);
    res.json(HEROES.find((h) => h.name === id ));
};