import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

//import { getHeroes } from './controllers/heroes.controller';
import { getProducts, getProduct, postProducts, putProducts, deleteProducts } from './controllers/productscontroller';
var usercontroller = require('./controllers/user.controller');
//import { postLogin, postRegister} from "./controllers/users.controller";

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// app.post('/login', postLogin);
// app.post('/register', postRegister);

//app.get('/heroes', getHeroes);

app.get('/products', getProducts);
app.get('/products/:name', getProduct);
app.post('/products', postProducts);
app.put('/products', putProducts);
app.delete('/products/:name', deleteProducts);

app.get('/users',usercontroller.getUsers);
app.post('/login',usercontroller.Login);
app.post('/register',usercontroller.Register);


app.listen(process.env.port || 3000);
console.log("server started");
